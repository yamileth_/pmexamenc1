package com.example.examenc1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import org.w3c.dom.Text

class activityRectangulo : AppCompatActivity() {

    private lateinit var lblBase: TextView;
    private lateinit var lblAltura: TextView;
    private lateinit var txtBase: EditText;
    private lateinit var txtAltura: EditText;
    private lateinit var btnCalcular: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnRegresar: Button;
    private lateinit var lblCalArea: TextView;
    private lateinit var txtCalcularArea: TextView;
    private lateinit var lblCalPerimetro: TextView;
    private lateinit var txtCalcularPerimetro: TextView;
    private lateinit var txtNombre: EditText;
    private var rectangulo = Rectangulo(0f,0f);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)

        iniciarComponentes()
        val bundle = intent.extras
        txtNombre.setText(bundle!!.getString("Nombre"))
        btnCalcular.setOnClickListener { calcular() }
        btnLimpiar.setOnClickListener { limpiar() }
        btnRegresar.setOnClickListener { regresar() }
    }

    fun limpiar(){
        txtBase.setText("")
        txtAltura.setText("")
        txtCalcularArea.setText("")
        txtCalcularPerimetro.setText("")
    }

    fun regresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Rectangulo")
        confirmar.setMessage("¿ Desea regresar ?")
        confirmar.setPositiveButton("Confirmar"){dialogoInterface,wich->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogoInterface,wich->}.show()

    }

    private fun iniciarComponentes(){
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtAltura = findViewById(R.id.txtAltura)
        txtBase = findViewById(R.id.txtBase)
        txtCalcularPerimetro = findViewById(R.id.txtCalcularPerimetro)
        txtCalcularArea = findViewById(R.id.txtCalcularArea)
        txtNombre = findViewById(R.id.txtNombre)
    }

    private fun calcular() {
        val base = txtBase.text.toString().trim()
        val altura = txtAltura.text.toString().trim()

        if (base.isNotEmpty() && altura.isNotEmpty()) {
            val baseR = base.toFloat()
            val alturaR = altura.toFloat()

            if (baseR > 0 && alturaR > 0) {
                rectangulo.base= baseR
                rectangulo.altura = alturaR
                txtCalcularArea.text = rectangulo.calcularArea().toString()
                txtCalcularPerimetro.text = rectangulo.calcularPermitro().toString()
            } else {
                Toast.makeText(this, "Los lados deben ser números positivos", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Por favor, asegúrese de llenar todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

}