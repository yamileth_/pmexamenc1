package com.example.examenc1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import android.content.DialogInterface

class MainActivity : AppCompatActivity() {

    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button
    private lateinit var txtNombre: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        btnEntrar.setOnClickListener { entrar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        txtNombre = findViewById(R.id.txtNombre)
    }

    private fun entrar() {
        val strNombre: String = txtNombre.text.toString()

        if (txtNombre.text.toString() == "") {
            Toast.makeText(applicationContext, "Nombre no ingresado", Toast.LENGTH_LONG).show()
        } else {
            val bundle = Bundle()
            bundle.putString("Nombre", strNombre)

            val intent = Intent(this@MainActivity, activityRectangulo::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            txtNombre.setText("")
        }
    }


    private fun salir() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Deseas cerrar la app?")
        builder.setPositiveButton("Sí") { dialog, which ->
            finish()
        }
        builder.setNegativeButton("No", null)
        builder.show()
    }

}