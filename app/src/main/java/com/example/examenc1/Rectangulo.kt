package com.example.examenc1

class Rectangulo {
    var base: Float = 0f
    var altura: Float = 0f

    constructor(base:Float, altura:Float){
        this.base = base;
        this.altura = altura;
    }

    fun calcularArea():Float{
        return altura * base;
    }

    fun calcularPermitro():Float{
        return (altura*2) + (base*2);
    }
}